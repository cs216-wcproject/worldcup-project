"""
Created on 12/4/2022

@author: admin
"""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

def avg_vs_actual_total_goals(total_goals_last, avg):
    dummy_avg = avg.rename(columns={"Team Name": "Team", "Predicted Goals": "Goals For"})
    dummy_avg["Type"] = "Average"
    dummy_act = total_goals_last.copy(deep=True)
    dummy_act["Type"] = "Actual"
    for_plot = pd.concat([dummy_avg, dummy_act], axis=0)
    chart = sns.catplot(x="Team", y="Goals For", kind="bar", data=for_plot, height=5, aspect=3, hue="Type").set(title='All Stage Goals: '
                                                                                                                     'Past Averages vs 2018')
    chart.set_xticklabels(rotation=30)
    plt.show()

def avg_vs_actual_group_stage_goals(goals_in_group_stage_last, avg):
    dummy_avg = avg.rename(columns={"Team Name": "Team", "Predicted Goals": "Goals For"})
    dummy_avg["Type"] = "Average"
    dummy_act = goals_in_group_stage_last.copy(deep=True)
    dummy_act["Type"] = "Actual"
    for_plot = pd.concat([dummy_avg, dummy_act], axis=0)
    chart = sns.catplot(x="Team", y="Goals For", kind="bar", data=for_plot, height=5, aspect=3, hue="Type").set(title='Group '
                                                                                                                                 'Stage '
                                                                                                                                 'Goals: '
                                                                                                                                 'Past Averages vs 2018')
    chart.set_xticklabels(rotation=30)
    plt.show()

def show_participation(filter_participation):
    chart = sns.catplot(x="Team Name", y="Number of Appearances", kind = "bar",
                        data=filter_participation, height=3, aspect=4).set(
    title ="Prior World Cup Participation for Teams at the 2018 World Cup")
    chart.set_xticklabels(rotation=50)
    plt.show()

def boxplot_avg_vs_actual_total_goals(total_goals_last, avg):
    dummy_avg = avg.rename(columns={"Team Name": "Team", "Predicted Goals":
        "Goals For"})
    dummy_avg["Type"] = "Average"
    dummy_act = total_goals_last.copy(deep=True)
    dummy_act["Type"] = "Actual"
    for_plot = pd.concat([dummy_avg, dummy_act], axis=0)
    #del for_plot["Team"]
    chart = sns.catplot(x="Type", y="Goals For", kind="box", data=for_plot).set(title ="All Stage Goals: Comparison of the Past Averages Dataset vs "
                                                                                       "the 2018 "
                                                                                       "Dataset")
    plt.show()

def boxplot_avg_vs_actual_group_stage_goals(goals_in_group_stage_last, avg):
    dummy_avg = avg.rename(columns={"Team Name": "Team", "Predicted Goals":
        "Goals For"})
    dummy_avg["Type"] = "Average"
    dummy_act = goals_in_group_stage_last.copy(deep=True)
    dummy_act["Type"] = "Actual"
    for_plot = pd.concat([dummy_avg, dummy_act], axis=0)
    chart = sns.catplot(x="Type", y="Goals For", kind="box", data=for_plot).set(title ="Group Stage Goals: Comparison of the Past Averages "
                                                                                       "Dataset vs "
                                                                                       "the 2018 "
                                                                                       "Dataset")
    plt.show()

if __name__ == '__main__':
    pass
