"""
Created on 12/4/2022

@author: admin
"""
import pandas as pd
import numpy as np
from scipy import stats

def make_last_wc_teams(total_goals_last):
    """
    Create series of all participants in 2018 world cup
    :param total_goals_last:
    :return: last_wc_teams
    """
    last_wc_teams = total_goals_last["Team"]
    return last_wc_teams

def compare(avg, actual):
    """
    :param avg: the dataframe containing the averages
    :param actual: the dataframe containing the actuals
    :return: t-statistic and p-value
    """
    totals_list = list(actual["Goals For"])
    avg_list = list(avg["Predicted Goals"])
    result = stats.ttest_ind(avg_list, totals_list)
    return result.pvalue

if __name__ == '__main__':
    pass
