"""
Created on 12/3/2022

@author: admin
"""
import pandas

def total_goals(all_world_cups):
    """
    Find total goals scored in all world cups
    :param all_world_cups:
    :return:
    """
    total_goals_all_home = all_world_cups.groupby("Home Team Name")["Home Team Goals"].sum().reset_index()
    total_goals_all_away = all_world_cups.groupby("Away Team Name")["Away Team Goals"].sum().reset_index()
    total_goals_all = total_goals_all_home.merge(total_goals_all_away, left_on="Home Team Name", right_on="Away Team Name", how="outer")
    add = total_goals_all.loc[78:82]
    add.rename(columns={"Away Team Goals": "Goals"}, inplace=True)
    add = add.drop(["Home Team Goals", "Home Team Name"], axis=1)
    total_goals_all["Goals"] = total_goals_all["Home Team Goals"] + total_goals_all["Away Team Goals"]
    total_goals_all = total_goals_all.drop(["Home Team Goals", "Away Team Goals", "Home Team Name"], axis=1).dropna()
    total_goals_all = total_goals_all.append(add)
    total_goals_all.rename(columns={"Away Team Name": "Team Name"}, inplace=True)
    add = total_goals_all.loc[59]["Goals"] + total_goals_all.loc[76]["Goals"]
    total_goals_all = total_goals_all[(total_goals_all["Team Name"] != "Yugoslavia") & (total_goals_all["Team Name"] !=
                                                                                          "Serbia and Montenegro")].reset_index(drop=True)
    total_goals_all.at[58, "Goals"] = add + total_goals_all.loc[58]["Goals"]
    #for i in range(len(total_goals_all)):
    ##    print(total_goals_all.loc[i, "Team Name"])
    return total_goals_all

def group_stage_goals(pattern, all_world_cups):
    """
    Find group stage goals scored in all world cups
    :param pattern:
    :param all_world_cups:
    :return:
    """
    goals_in_group_stage_all = all_world_cups[all_world_cups["Stage"].str.match(pattern)]
    goals_in_group_stage_all_home = goals_in_group_stage_all.groupby("Home Team Name")["Home Team Goals"].sum().reset_index()
    goals_in_group_stage_all_away = goals_in_group_stage_all.groupby("Away Team Name")["Away Team Goals"].sum().reset_index()
    goals_in_group_stage_all = goals_in_group_stage_all_home.merge(goals_in_group_stage_all_away, left_on="Home Team Name",
                                                                   right_on="Away Team Name", how="outer")
    add = goals_in_group_stage_all.loc[77:80]
    add.rename(columns={"Away Team Goals": "Goals"}, inplace=True)
    add = add.drop(["Home Team Goals", "Home Team Name"], axis=1)
    goals_in_group_stage_all["Goals"] = goals_in_group_stage_all["Home Team Goals"] + goals_in_group_stage_all["Away Team Goals"]
    goals_in_group_stage_all = goals_in_group_stage_all.drop(["Home Team Goals", "Away Team Goals", "Home Team Name"], axis=1).dropna()
    goals_in_group_stage_all = goals_in_group_stage_all.append(add)
    goals_in_group_stage_all.rename(columns={"Away Team Name": "Team Name"}, inplace=True)
    return goals_in_group_stage_all

def participation(all_world_cups):
    """
    Find total participation from all teams
    :param all_world_cups:
    :return:
    """
    dict = {}
    for i in range(len(all_world_cups)):
        key = all_world_cups.loc[i, "Home Team Name"]
        if key in dict:
            dict[key].append(all_world_cups.loc[i, "Year"])
        else:
            dict[key] = [all_world_cups.loc[i, "Year"]]
        key = all_world_cups.loc[i, "Away Team Name"]
        if key in dict:
            dict[key].append(all_world_cups.loc[i, "Year"])
        else:
            dict[key] = [all_world_cups.loc[i, "Year"]]
    for key in dict:
        dict[key] = len(list(set(dict[key])))
    total_participation = pandas.DataFrame(list(dict.items()))
    total_participation.rename(columns={0: "Team Name", 1: "Number of Appearances"}, inplace=True)
    add = total_participation.loc[4]["Number of Appearances"] + total_participation.loc[74]["Number of Appearances"]
    total_participation = total_participation[(total_participation["Team Name"] != "Yugoslavia") & (total_participation["Team Name"] !=
                                              "Serbia and Montenegro")].reset_index(drop=True)
    total_participation.at[78, "Number of Appearances"] = add + total_participation.loc[78]["Number of Appearances"]
    return total_participation

def last_world_cup_total(last_world_cup):
    """
    Find total goals scored in 2018 world cup
    :param last_world_cup:
    :return:
    """
    total_goals_last = last_world_cup.groupby("Team")["Goals For"].sum().reset_index()
    return total_goals_last

def last_world_cup_group_stage(pattern, last_world_cup):
    """
    Find group stage goals scored in 2018 world cup
    :param pattern:
    :param last_world_cup:
    :return:
    """
    goals_in_group_stage_last = last_world_cup[last_world_cup["Group"].str.match(pattern)]
    goals_in_group_stage_last = goals_in_group_stage_last.groupby("Team")["Goals For"].sum().reset_index()
    return goals_in_group_stage_last


if __name__ == '__main__':
    pass
