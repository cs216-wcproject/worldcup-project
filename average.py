"""
Created on 12/3/2022

@author: admin
"""
import pandas as pd

def filter(wc_teams, df):
    filter_df = df[df["Team Name"].isin(wc_teams)].reset_index(drop=True)
    filter_df = filter_df.append(pd.DataFrame(data={'Team Name': ["Panama", "Iceland"], list(filter_df.columns.values)[1]: [0, 0]}))
    filter_df.sort_values(by=["Team Name"], ignore_index=True, inplace=True)
    return filter_df

def make_average(filter_goals_all, filter_participation):
    avg = pd.DataFrame(data={"Team Name": list(filter_goals_all["Team Name"]), "Predicted Goals": (filter_goals_all[
                                                                                                      "Goals"]/filter_participation[
                                                                                                             "Number of "
                                                                                                                   "Appearances"])})
    avg.fillna(0, inplace=True)
    return avg


if __name__ == '__main__':
    pass
