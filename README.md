# WorldCup-Project

## Name
Analyzing the Uniqueness of the 2018 FIFA World Cup

## Description
Every four years, nations from across the globe gather together in anticipation of the world’s largest sporting event: the Fédération internationale de Football Association (FIFA) World Cup. For just over the span of a month, millions watch in intrigue as the best and brightest soccer stars from each qualifying country compete to determine who will bear the next title of world champion.
This project seeks to understand how previous FIFA World Cups compare to that of the 2018 FIFA World Cup, the last such completed tournament. Given that performance in soccer is largely dictated by the number of goals that a team scores, goals were the important metric.
To explore this inquiry, we compared both the overall number of goals and the group stage number of goals that each team scored in the 2018 World Cup with the number of goals that they averaged in previous World Cups. The reason we chose to look at both group stage and overall goals in the tournament was that only using group stage goals normalized the dataset. This is because every participant is guaranteed to play the same number of World Cup group stage games (3), while additional knock-out round games are not guaranteed. The averages will be determined by gathering the number of goals scored and dividing by the number of World Cups participated in, which will be done for each team that qualified for the 2018 World Cup. 

## Roadmap
The next step in future work for our project would be to perform the same analysis yet for prior world cups performances and averages. For example, comparing the average of world cup goals from 1930-2010 to 2014 world cup performance to obtain p-values, and then from 1930-2006 compared to 2010 world cup performance and so forth. Obtaining p-values for each world cup back to a certain year would allow us to expand on our existing research questions and potentially create new ones. For example, perhaps we could conclude that world cups with a certain combination of teams start to become ‘predictable’ in regards to the number of goals that each team scores. From there we could also potentially examine the relationship between goals scored in a group stage and the outcome of the group. This could include team-weighting based on goals, appearances, wins, and a host of other data points.

## Authors and acknowledgment
Brooks Robinson - pba6
Aiden Fox - agf19
Michael Murata - mm875
Jordan Greene - jcg96

## Project status
Complete
