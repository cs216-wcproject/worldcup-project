"""
Created on 11/8/2022

@author: Brooks Robinson, Michael Murata, Jordan Greene, Aiden Fox
"""

import numpy as np
import pandas as pd
import re
import find
import compare
import average
import visualizations

def clean_all_world_cups(data):
    data = data[["Year", "Home Team Name", "Home Team Goals", "Away Team Goals", "Away Team "
                                                                                 "Name", "Stage"]]
    column_names = data.columns.values
    data["Home Team Name"] = data["Home Team Name"].str.lstrip("rn\">")
    data["Away Team Name"] = data["Away Team Name"].str.lstrip("rn\">")
    return data

def clean_last_world_cup(data):
    column_names = data.columns.values
    data = data[["Group", "Team", "Opponent", "Home/Away", "Score", "WDL", "Pens?",
                 "Goals For", "Goals Against", "Pen Shootout For", "Pen Shootout Against"]]
    return data


if __name__ == '__main__':
    #Work with dataset for all world cups
    all_world_cups = clean_all_world_cups(pd.read_csv("WorldcupMatches.csv"))
    #Find all goals scored in all world cups
    total_goals_all = find.total_goals(all_world_cups)
    #print("Total Goals in World Cups 1930-2014: \n", total_goals_all)
    #Find goals just scored in group stage
    pattern = re.compile("Group [1-9A-Z]|Preliminary round")
    goals_in_group_stage_all = find.group_stage_goals(pattern, all_world_cups)
    #print("Goals Scored Only in Group Stage in World Cups 1930-2014: \n", goals_in_group_stage_all)
    #Find total world cup participation
    total_participation = find.participation(all_world_cups)
    #print("Total Participation in World Cups 1930-2014: \n", total_participation)

    #work with dataset for 2018 world cup
    last_world_cup = clean_last_world_cup(pd.read_csv("world_cup_2018_stats.csv"))
    #Find all goals scored in 2018 world cup
    total_goals_last = find.last_world_cup_total(last_world_cup)
    #print("Total Goals in 2018 World Cup: \n", total_goals_last)
    #Find goals just scored in group stage in 2018 world cup
    pattern = re.compile("[A-Z]$")
    goals_in_group_stage_last = find.last_world_cup_group_stage(pattern, last_world_cup)
    #print("Goals Scored Only in Group Stage in 2018 World Cup: \n", goals_in_group_stage_last)

    #Create arrays of participating teams in 2018 (last) world cup
    last_wc_teams = compare.make_last_wc_teams(total_goals_last)
    #Create averages for comparison against the 2018 world cup
    filter_goals_all = average.filter(last_wc_teams, total_goals_all)
    filter_goals_group_stage = average.filter(last_wc_teams, goals_in_group_stage_all)
    filter_participation = average.filter(last_wc_teams, total_participation)
    avg_all = average.make_average(filter_goals_all, filter_participation)
    avg_group_stage = average.make_average(filter_goals_group_stage, filter_participation)
    #Compare the two datasets
    pval_total_goals = compare.compare(avg_all, total_goals_last)
    pval_group_stage_goals = compare.compare(avg_group_stage, goals_in_group_stage_last)
    #Create comparison visualization for 2018
    visualizations.avg_vs_actual_total_goals(total_goals_last, avg_all)
    visualizations.avg_vs_actual_group_stage_goals(goals_in_group_stage_last, avg_group_stage)
    visualizations.show_participation(filter_participation)
    visualizations.boxplot_avg_vs_actual_total_goals(total_goals_last, avg_all)
    visualizations.boxplot_avg_vs_actual_group_stage_goals(goals_in_group_stage_last, avg_group_stage)

    print("The p-value from the t-test for total tournament goals: ",pval_total_goals, "\n")
    print("The p-value from the t-test for group-stage goals: ", pval_group_stage_goals, "\n")


